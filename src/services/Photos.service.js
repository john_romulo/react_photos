import { PHOTOS_API_PROPERTIES } from '../constants/constants';
import { BaseService } from './Base.service';

export class PhotosService {
    static getAllPhotosFromServer(page) {
        let param = {
            page: page,
            per_page: 12,
            order_by: 'popular',
            client_id: PHOTOS_API_PROPERTIES.ACCESS_KEY
        };
        return BaseService.get('photos', param);
    }
}
