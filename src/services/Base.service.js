import Axios from 'axios';
import { BASE_URL } from '../constants/constants';


export class BaseService {

    //metodos baseados no REST FULL
    static post() {

    }

    static get(endpoint, params) {
        return new Promise((resolve, reject) => {
            Axios.get(`${BASE_URL}/${endpoint}`, {
                params: params
            })
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    static put() {

    }

    static delete() {

    }
}