import React, { Component } from 'react';
import styles from './Photo.css';
import { bindActionCreators } from 'redux';
import * as actions from '../../store/actions/photos.actions';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import LoadingComponent from '../../components/loading/Logind.component';
import CardComponent from '../../components/card/Card.component';
import ModalPhotos from '../../components/modal-photo/ModalPhoto.component';



class PhotosPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            photoSelect: {
                urls: {},
                user: { profile_image: {} }
            }
        }

    }

    componentDidMount() {
        this.getAllPhotosFromServer(1);
    }

    getAllPhotosFromServer(page) {
        this.props.actionGetAllPhotos(page);
    }

    handleOpen = (photo) => {
        this.setState({
            open: true,
            photoSelect: photo
        });
        console.log("photo", photo)
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        return (
            <div className={styles.PhotoPage}>
                {
                    this.props.photos.loading ?
                        <LoadingComponent /> :
                        <InfiniteScroll className={styles.infiniteScrollComponent}
                            initialLoad={false}
                            pageStart={1}
                            loadMore={this.getAllPhotosFromServer.bind(this)}
                            hasMore={true}>
                            {
                                this.props.photos.data.map((photo, index) => {
                                    return (
                                        <div key={index} onClick={() => this.handleOpen(photo)}>
                                            <CardComponent photo={photo} />
                                        </div>
                                    );
                                })
                            }
                        </InfiniteScroll>
                }
                <ModalPhotos photo={this.state.photoSelect} open={this.state.open} handleClose={this.handleClose.bind(this)} />
            </div >
        );
    }

}

const mapStateToProps = state => ({
    photos: state.photosReducer
});


const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PhotosPage);
