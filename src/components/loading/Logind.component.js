import React from 'react';
import styles from './Loading.css';


const LoadingComponent = () => {
    return (
        <div className={styles.LoadingComponent}>
            <div className={styles.body}>
                <span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                <div className={styles.base}>
                    <span></span>
                    <div className={styles.face}></div>
                </div>
            </div>
            <div className={styles.longfazers}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <h1>Carregando...</h1>
        </div>
    );
}

export default LoadingComponent;