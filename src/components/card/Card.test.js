import React from 'react';
import CardComponent from './Card.component';
import { shallow, mount } from 'enzyme';

const photo = {
    "id": "Mg0W1N_yDv0",
    "created_at": "2018-09-05T13:06:03-04:00",
    "updated_at": "2018-09-08T12:51:25-04:00",
    "width": 4016,
    "height": 6016,
    "color": "#181720",
    "description": null,
    "urls": {
        "raw": "https://images.unsplash.com/photo-1536167038724-17be8c5e6876?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjM0OTM4fQ&s=45a656351425f48f29401136a5c1521c", "full": "https://images.unsplash.com/photo-1536167038724-17be8c5e6876?ixlib=rb-0.3.5&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjM0OTM4fQ&s=dca7ddb496f2e7b302d6afabee016e48", "regular": "https://images.unsplash.com/photo-1536167038724-17be8c5e6876?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM0OTM4fQ&s=036663a82e2f6e128fff10e606c5d2b8", "small": "https://images.unsplash.com/photo-1536167038724-17be8c5e6876?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjM0OTM4fQ&s=a3986c85806412583decaf7af8293d89", "thumb": "https://images.unsplash.com/photo-1536167038724-17be8c5e6876?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM0OTM4fQ&s=fda78b6d791adcd5c74a6c2ab25f4c5d"
    },
    "links":
    {
        "self": "https://api.unsplash.com/photos/Mg0W1N_yDv0",
        "html": "https://unsplash.com/photos/Mg0W1N_yDv0",
        "download": "https://unsplash.com/photos/Mg0W1N_yDv0/download",
        "download_location": "https://api.unsplash.com/photos/Mg0W1N_yDv0/download"
    },
    "categories": [],
    "sponsored": false,
    "likes": 169,
    "liked_by_user": false,
    "current_user_collections": [],
    "slug": null,
    "user": {
        "id": "zKou8F1Vm1o",
        "updated_at": "2018-09-19T10:27:52-04:00",
        "username": "evieshaffer",
        "name": "Evie Shaffer",
        "first_name": "Evie",
        "last_name": "Shaffer",
        "twitter_username": null,
        "portfolio_url": "http://evies.com",
        "bio": "Lover of art and nature. Tireless Seeker of truth. Creating beauty and pointing to the Beauty-giver in the process. Find me on instagram: @evieshaffer",
        "location": "Tulsa, Oklahoma",
        "links": {
            "self": "https://api.unsplash.com/users/evieshaffer",
            "html": "https://unsplash.com/@evieshaffer",
            "photos": "https://api.unsplash.com/users/evieshaffer/photos",
            "likes": "https://api.unsplash.com/users/evieshaffer/likes",
            "portfolio": "https://api.unsplash.com/users/evieshaffer/portfolio",
            "following": "https://api.unsplash.com/users/evieshaffer/following",
            "followers": "https://api.unsplash.com/users/evieshaffer/followers"
        },
        "profile_image": {
            "small": "https://images.unsplash.com/profile-fb-1515003070-191da6a69ab7.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32&s=30ffcdf062d377fbaa16b31c5384883a", "medium": "https://images.unsplash.com/profile-fb-1515003070-191da6a69ab7.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64&s=53c6172c2f9b9803e4479d6c16540a08", "large": "https://images.unsplash.com/profile-fb-1515003070-191da6a69ab7.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=128&w=128&s=110620b33e0ce018faf22e0748af02fe"
        },
        "instagram_username": "evieshaffer",
        "total_collections": 4,
        "total_likes": 97,
        "total_photos": 42
    }
}

it('should mount in a full DOM', function () {
    expect(mount(<CardComponent photo={photo} />).contains(
        <span>Evie Shaffer</span>
    )).toBe(true);
});