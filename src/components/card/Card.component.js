import React from 'react';
import styles from './Card.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Img from 'react-image';
import { SkeletonImg } from 'react-js-skeleton';
import PropTypes from 'prop-types';

const CardComponent = ({ photo }) => {
    return (
        <Card className={styles.card}>
            <CardHeader className={styles.card_hearder}
                avatar={
                    <Avatar aria-label="Recipe" className={styles.card_avatar}>
                        <Img className={styles.card_avatar_image}
                            src={photo.user.profile_image.medium}
                            loader={<SkeletonImg />}
                            unloader={<SkeletonImg />}
                        />
                    </Avatar>
                }
                title={
                    <div>
                        <span className={styles.card_user_full_name}>
                            {photo.user.name}
                        </span>
                    </div>
                }
                subheader={
                    <div>
                        <span className={styles.card_user_name}>
                            {`@${photo.user.username}  `}
                        </span>
                        <span className={styles.card_user_location}>
                            {photo.user.location ? `| ${photo.user.location}` : ''}
                        </span>
                    </div>
                }
            />
            <Img className={styles.card_image}
                src={photo.urls.small}
                loader={
                    <SkeletonImg width={350}
                        heightSkeleton={'55%'} />
                }
                unloader={
                    <SkeletonImg width={350}
                        heightSkeleton={'55%'} />
                }
            />
            <CardContent className={styles.card_content}>
                <Typography component="span" className={styles.card_typography} >
                    <Icon className={styles.card_typography_icon_like}>
                        favorite
                    </Icon>
                    <span className={styles.card_typography_text_like}>
                        {photo.likes}
                    </span>
                </Typography>
            </CardContent>
        </Card>
    );
};


CardComponent.propTypes = {
    photo: PropTypes.object.isRequired,
};

export default CardComponent;
