import React from 'react';
import styles from './ModalPhoto.css';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Img from 'react-image';
import { SkeletonImg } from 'react-js-skeleton';
import Icon from '@material-ui/core/Icon';

const ModalPhotos = ({ open, handleClose, photo }) => {
    return (
        <div>
            <Modal className={styles.modal_content}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={handleClose}>
                <div className={styles.paper}>
                    <div>
                        <Icon>
                            close
                        </Icon>
                    </div>
                    <Img className={styles.modal_image}
                        src={photo.urls.regular}
                        loader={
                            <SkeletonImg width={'100%'} />
                        }
                        unloader={
                            <SkeletonImg width={'100%'} />
                        }
                    />
                    <Typography variant="subheading" id="simple-modal-description">
                        silhouette photography of person
                    </Typography>
                </div>
            </Modal>
        </div>
    );
}

ModalPhotos.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired
};


export default ModalPhotos;