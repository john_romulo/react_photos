export const BASE_URL = 'https://api.unsplash.com/';

export const TYPE_ACTIONS = {
    REQUEST_PHOTOS_LIST: "request_photos_list",
    GET_ALL_PHOTOS: "get_all_photos",
    SUCCESS_PHOTOS_LIST: "success_photos_list",
    FAILURE_PHOTOS_LIST: "failure_photos_list"
}

export const PHOTOS_API_PROPERTIES = {
    ACCESS_KEY: "ae417a52219f630c6dc497766cd0800241662265a8494dffc147c503430263bd",
}