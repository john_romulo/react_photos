import { List } from 'immutable';
import { TYPE_ACTIONS } from '../../constants/constants';

const INITIAL_STATE = {
    data: new List(),
    loading: false,
    erros: false
}

export default function photosReducer(state = INITIAL_STATE, action) {

    switch (action.type) {
        case TYPE_ACTIONS.SUCCESS_PHOTOS_LIST:
            const newPhotos = state.data.concat(action.photos);
            return { data: newPhotos, loading: false, error: false }
        case TYPE_ACTIONS.FAILURE_PHOTOS_LIST:
            return { data: [], loading: false, error: true };
        case TYPE_ACTIONS.REQUEST_PHOTOS_LIST:
            return { ...state, loading: true };
        default:
            return state;
    }
}