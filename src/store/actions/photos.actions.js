import { TYPE_ACTIONS } from '../../constants/constants';

export function actionGetAllPhotos(page) {
    return { type: TYPE_ACTIONS.GET_ALL_PHOTOS, page };
}
