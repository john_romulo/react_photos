//import { delay } from 'redux-saga';
import { takeLatest, put, call, all } from 'redux-saga/effects';
import { PhotosService } from '../../services/Photos.service';
import { TYPE_ACTIONS } from '../../constants/constants';

function* getPhotosList(action) {
    try {
        if (action.page === 1) {
            yield put({ type: TYPE_ACTIONS.REQUEST_PHOTOS_LIST });
        }
        const photos = yield call(PhotosService.getAllPhotosFromServer, action.page);
        yield put({ type: TYPE_ACTIONS.SUCCESS_PHOTOS_LIST, photos: photos.data });
    } catch (err) {
        yield put({ type: TYPE_ACTIONS.FAILURE_PHOTOS_LIST });
    }
}

export default function* root() {
    yield all([
        takeLatest(TYPE_ACTIONS.GET_ALL_PHOTOS, getPhotosList)
    ]);
}