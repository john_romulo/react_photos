import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import PhotosPage from './pages/photos/Photos.page';
import styles from './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className={styles.app_content}>
          <PhotosPage />
        </div>
      </Provider>
    );
  }
}

export default App;
